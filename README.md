# SatNOGS Router

SatNOGS Router software.
This package is responsible for routing artifacts of satellite observations collected by SatNOGS Project.

## Install and Contribute

To install SatNOGS Router, simply:

```
  $ pip install satnogs-router
```

To get the latest development version:

```
  $ git clone https://gitlab.com/librespacefoundation/satnogs/satnogs-router.git
  $ cd satnogs-router
  $ pip install -e .
```

For more information see our [documentation](https://docs.satnogs.org/projects/satnogs-router). (TBD)

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/satnogs/satnogs-router) and all Merge Request should happen there.


## Join

[![irc](https://img.shields.io/badge/Matrix-%23satnogs:matrix.org-blue.svg)](https://riot.im/app/#/room/#satnogs:matrix.org)
[![irc](https://img.shields.io/badge/IRC-%23satnogs%20on%20freenode-blue.svg)](https://webchat.freenode.net/?channels=satnogs)
[![irc](https://img.shields.io/badge/forum-discourse-blue.svg)](https://community.libre.space/c/satnogs)


## Current Development

[![build](https://gitlab.com/librespacefoundation/satnogs/satnogs-router/badges/master/build.svg)](https://gitlab.com/librespacefoundation/satnogs/satnogs-router/commits/master)


## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202020--2021-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
