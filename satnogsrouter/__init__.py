"""
SatNOGS Router module initialization
"""
import logging
import logging.config

from satnogsrouter import router, settings

from ._version import get_versions

logging.basicConfig(format=settings.LOG_FORMAT,
                    level=getattr(logging, settings.LOG_LEVEL),
                    datefmt=settings.LOG_DATE_FORMAT)

LOGGER = logging.getLogger(__name__)


def main():
    """
    Main function
    """

    LOGGER.info('Starting router.proxy...')
    router.run_proxy()


__version__ = get_versions()['version']
del get_versions
