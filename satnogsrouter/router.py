"""
SatNOGS Router Proxy Module

This module allows to proxy and monitor artifact frames coming from SatNOGS DB
"""

import logging

import zmq
from zmq import devices

from satnogsrouter import settings


def run_proxy():
    """ Proxy main method """

    # Prepare context
    context = zmq.Context()

    # Prepare monitoring Subscriber
    monitor_sub = context.socket(zmq.SUB)
    monitor_sub.setsockopt(zmq.SUBSCRIBE, b'')
    monitor_sub.connect(settings.ZEROMQ_MONITOR_URI)
    logging.info('Monitor Subscriber connects to %s', settings.ZEROMQ_MONITOR_URI)

    proxy = devices.ThreadProxy(zmq.XSUB, zmq.XPUB, zmq.PUB)

    # Prepare frontend Subscriber
    proxy.bind_in(settings.ZEROMQ_PROXY_XSUB_URI)
    logging.info('Subscriber binds to %s', settings.ZEROMQ_PROXY_XSUB_URI)

    # Prepare backend Publisher
    proxy.bind_out(settings.ZEROMQ_PROXY_XPUB_URI)
    proxy.setsockopt_out(zmq.XPUB_VERBOSER, 1)
    logging.info('Publisher binds to %s', settings.ZEROMQ_PROXY_XPUB_URI)

    # Prepare monitoring Publisher
    proxy.bind_mon(settings.ZEROMQ_MONITOR_URI)
    logging.info('Monitor Publisher binds to %s', settings.ZEROMQ_MONITOR_URI)

    proxy.start()

    while True:
        msg = monitor_sub.recv_multipart()
        logging.info(msg)
