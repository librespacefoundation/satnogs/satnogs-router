"""
SatNOGS Router settings file
"""
from os import environ

from dotenv import load_dotenv


def _cast_or_none(func, value):
    try:
        return func(value)
    except (ValueError, TypeError):
        return None


try:
    load_dotenv()
except EnvironmentError:
    # XXX: Workaround for readthedocs  # pylint: disable=fixme
    pass

# ZeroMQ Proxy
ZEROMQ_PROXY_XSUB_PROTOCOL = environ.get('ZEROMQ_PROXY_XSUB_URI', 'tcp://')
ZEROMQ_PROXY_XSUB_ADDRESS = environ.get('ZEROMQ_PROXY_XSUB_URI', '0.0.0.0')
ZEROMQ_PROXY_XSUB_PORT = environ.get('ZEROMQ_PROXY_XSUB_URI', '5555')
ZEROMQ_PROXY_XSUB_URI = (ZEROMQ_PROXY_XSUB_PROTOCOL + ZEROMQ_PROXY_XSUB_ADDRESS + ':' +
                         ZEROMQ_PROXY_XSUB_PORT)
ZEROMQ_PROXY_XPUB_PROTOCOL = environ.get('ZEROMQ_PROXY_XPUB_URI', 'tcp://')
ZEROMQ_PROXY_XPUB_ADDRESS = environ.get('ZEROMQ_PROXY_XPUB_URI', '0.0.0.0')
ZEROMQ_PROXY_XPUB_PORT = environ.get('ZEROMQ_PROXY_XPUB_URI', '5560')
ZEROMQ_PROXY_XPUB_URI = (ZEROMQ_PROXY_XPUB_PROTOCOL + ZEROMQ_PROXY_XPUB_ADDRESS + ':' +
                         ZEROMQ_PROXY_XPUB_PORT)
ZEROMQ_MONITOR_PROTOCOL = environ.get('ZEROMQ_MONITOR_PUB_URI', 'tcp://')
ZEROMQ_MONITOR_ADDRESS = environ.get('ZEROMQ_MONITOR_PUB_URI', '0.0.0.0')
ZEROMQ_MONITOR_PORT = environ.get('ZEROMQ_MONITOR_PUB_URI', '5565')
ZEROMQ_MONITOR_URI = ZEROMQ_MONITOR_PROTOCOL + ZEROMQ_MONITOR_ADDRESS + ':' + ZEROMQ_MONITOR_PORT

# Logging configuration
LOG_FORMAT = '%(asctime)s %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
LOG_LEVEL = environ.get('SATNOGS_LOG_LEVEL', 'INFO')
